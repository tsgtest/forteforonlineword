﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Xml;

namespace ForteForOnlineWordWeb
{
    public class SegmentParams
    {
        public string ID { get; set; }
        public string Prefill { get; set; }
    }

    public class ForteServerController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
        [HttpPost()]
        public string CreateDocument(SegmentParams oParams)
        {
            try
            {
                /* <ForteInstruction type=CreateDocument>
                *      <ID>3039</ID>
                *      <Prefill>
                           <Authors>2143<\Authors>
                           <AuthorInitials>LAA</AuthorInitials>
                           <CC><FULLNAME Index='1'>Jerry Wolfe</FULLNAME></CC>
                           <BCC><FULLNAME Index='1'>Linda J. Sackett</FULLNAME></BCC>
                           <ClientMatterNumber>53001.001</ClientMatterNumber>
                           <ClosingPhrase>Sincerely,</ClosingPhrase>
                           <DateFormat>MMMM d, yyyy</DateFormat>
                           <DeliveryPhrases>By Certified Mail
                           Return Receipt Requested</DeliveryPhrases>
                           <Enclosures>Enclosure</Enclosures>
                           <HeaderDeliveryPhrases>True</HeaderDeliveryPhrases>
                           <IncludeAdmittedIn>False</IncludeAdmittedIn>
                           <IncludeEMail>True</IncludeEMail>
                           <IncludeFax>False</IncludeFax>
                           <IncludeName>True</IncludeName>
                           <IncludePhone>True</IncludePhone>
                           <LetterheadIncludeTitle>False</LetterheadIncludeTitle>
                           <Page2HeaderText>Ms. Laura Halliday
                           Ms. Valerie Melville</Page2HeaderText>
                           <ReLine>Subject line</ReLine>
                           <Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1'>Ms. Laura Halliday</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1'>Project Manager</TITLE><COMPANY Index='1'>Legal MacPac</COMPANY><COREADDRESS Index='1'>1430 Judah Street, #2
                           San Francisco, CA  94122</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2'>Ms. Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2'>Product and Client Services Manager</TITLE><COMPANY Index='2'>The Sackett Group, Inc.</COMPANY><COREADDRESS Index='2'>7367 Los Brazos
                           San Diego, CA  92127</COREADDRESS></Recipients>
                           <Salutation>Dear Laura and Valerie:</Salutation>
                           <AuthorName>Louis A. Armstrong</AuthorName>
                           <IncludeFirmName>True</IncludeFirmName>
                           <IncludeTitle>True</IncludeTitle>
                           <TypistInitials>ljs</TypistInitials>
                *      </Prefill>
                *      <EmailAddress>DFisherman@thesackettgroup.com</EmailAddress>
                *      <OutputFormat>docx</OutputFormat>
                * </ForteInstruction> */

                //add client functionality to Word, which will allow users to select segment and prefill, then send to server
                TSG.ForteServer.ForteServerClient oServer = new TSG.ForteServer.ForteServerClient();

                //string xID1 = "6707";
                //string xID2 = "6709";
                //string xPrefill1 = "<Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index=\"1\" UNID=\"0\">Doug Miller</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"1\" UNID=\"0\">Developer</TITLE><COMPANY Index=\"1\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"1\" UNID=\"0\">28 6th Ave. &#13;&#10;NY, NY 10003</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index=\"2\" UNID=\"0\">Linda Sackett</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index=\"2\" UNID=\"0\">Operations Manager</TITLE><COMPANY Index=\"2\" UNID=\"0\">TSG</COMPANY><COREADDRESS Index=\"2\" UNID=\"0\">Her address &#13;&#10;Her city, state, and zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString><NewVariable1>900 Fifth Avenue, Suite 2500&#13;&#10;New York, New York  10003</NewVariable1></Recipients><NewVariable2>this is the variable 2 value</NewVariable2><Reline><Standard Format=\"0\" Default=\"Alternate Insurance\">This is the reline value</Standard><Special Format=\"0\" Default=\"Alternate Insurance\"><Label Index=\"1\">Claimant</Label><Value Index=\"1\">c</Value><Label Index=\"2\">Date of Claim</Label><Value Index=\"2\">d</Value><Label Index=\"3\">Company Name</Label><Value Index=\"3\">c</Value><Label Index=\"4\">Action Number</Label><Value Index=\"4\">a</Value></Special></Reline><AuthorInitials>DCF</AuthorInitials><ReLine2><Standard Format=\"0\" Default=\"Alternate Insurance\">reline 2  value akls;dfj ads ;ruwpqeo ruqoiewur o qeorpoie q r pqoiewur qw erpoiquweqe pqiweu pqo ewiuq pirqpeiuqper qepur qpweqeo r periuq ep qwpriu wrq er poe rqerpqiew rq ewroipqwueroipqi roiru</Standard></ReLine2><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";
                //string xPrefill2 = "<IncludeTitle>true</IncludeTitle><Recipients><FULLNAMEWITHPREFIXANDSUFFIX Index='1' UNID='0'>Daniel Fisherman</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='1' UNID='0'>Developer</TITLE><COMPANY Index='1' UNID='0'>TSG</COMPANY><COREADDRESS Index='1' UNID='0'>my address&#13;&#10;my city, my state my zip</COREADDRESS><FULLNAMEWITHPREFIXANDSUFFIX Index='2' UNID='0'>Valerie Melville</FULLNAMEWITHPREFIXANDSUFFIX><TITLE Index='2' UNID='0'>Manager</TITLE><COMPANY Index='2' UNID='0'>TSG</COMPANY><COREADDRESS Index='2' UNID='0'>her address&#13;&#10;her city, her state her zip</COREADDRESS><CIDetailTokenString>[CIDetail__FullNameWithPrefixAndSuffix][CIDetail__TitleIfBusiness][CIDetail__CompanyIfBusiness][CIDetail__COREADDRESS]</CIDetailTokenString></Recipients><UseTableFormat>false</UseTableFormat><Reline><Standard Format='0' Default='Alternate Insurance'>this is the reline</Standard><Special Format='0' Default='Alternate Insurance'><Label Index='1'>Claimant</Label><Value Index='1'>a</Value><Label Index='2'>Date of Claim</Label><Value Index='2'>b</Value><Label Index='3'>Company Name</Label><Value Index='3'>c</Value><Label Index='4'>Action Number</Label><Value Index='4'>d</Value></Special></Reline><Subject>This is the subject line</Subject><Agreed>true</Agreed><DateFormat>MMMM dd, yyyy</DateFormat><ExecuteonBlock>true</ExecuteonBlock><TitleProperty>doc title</TitleProperty><CustomTitle>custom doc title</CustomTitle><DocumentVariableValue>dv custom 1 var text</DocumentVariableValue><SetLanguage>true</SetLanguage><FontName>Calibri</FontName><FontSize>12.0</FontSize><BodyTextFirstLineIndent>1.0</BodyTextFirstLineIndent><BodyTextAlignment>0</BodyTextAlignment><AuthorName>Daniel C. Fisherman</AuthorName><Culture>1033</Culture><Authors>9358.0¦-1¦Daniel C. Fisherman¦7659872|</Authors>";
                //string xID = null;
                //string xPrefill = null;

                string strHostName = Dns.GetHostName();
                Console.WriteLine("Local Machine's Host Name: " + strHostName);
                // Then using host name, get the IP address list..
                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
                string addr = "momshead@gmail.com";

                foreach (var ip in ipEntry.AddressList)
                {
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && ip.ToString().StartsWith("10."))
                    {
                        addr = strHostName + " (IP: " + ip.ToString() + "; Doc ID: " + oParams.ID + ")";
                        break;
                    }
                }

                string xPrefill = ConvertXmlToDelimitedString("<Prefill>" + oParams.Prefill + "</Prefill>");

                string instruction = "<ForteInstructions><ForteInstruction type=\"CreateDocument\"><ID>" + oParams.ID + "</ID><User>Daniel Fisherman</User><Prefill>" + xPrefill +
                    "</Prefill><EmailAddress>" + addr + "</EmailAddress><OutputFormat>docx</OutputFormat></ForteInstruction></ForteInstructions>";
                string xText = oServer.ProcessInstructionSet(instruction);

                //string xFileName = null;
                //string xDir = LMP.Registry.GetMacPac10Value("SaveDirectoryUrl");
                //string xDir = @"C:\Users\Daniel Fisherman\Documents\Work\MacPac\Forte Server\Docs";

                //if (!xDir.EndsWith("\\"))
                //{
                //    xDir = xDir + "\\";
                //}

                //xFileName = xDir + System.Guid.NewGuid().ToString() + ".docx";

                //string xXML = null;

                //using (FileStream oFS = File.Create(xFileName))
                //{
                //    byte[] b = Convert.FromBase64String(xText);
                //    oFS.Write(b, 0, b.Length);
                //}

                return xText;
            }
            catch (System.Exception oE)
            {
                return oE.Message + '\v' + oE.StackTrace;
            }
        }

        private string ConvertXmlToDelimitedString(string xPrefill)
        {
            StringBuilder oSB = new StringBuilder();

            XmlDocument oXml = new XmlDocument();
            oXml.LoadXml(xPrefill);

            XmlNodeList oElements = oXml.DocumentElement.ChildNodes;
            foreach (XmlNode element in oElements)
            {
                oSB.AppendFormat("{0}¤{1}Þ", element.Name, element.InnerXml);
            }

            oSB.Remove(oSB.Length - 1, 1);
            return oSB.ToString();
        }
    }
}

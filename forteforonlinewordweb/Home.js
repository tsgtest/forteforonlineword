﻿/// <reference path="/Scripts/FabricUI/MessageBanner.js" />


(function () {
    "use strict";

    var messageBanner;

    // The initialize function must be run each time a new page is loaded.
    Office.initialize = function (reason) {
        $(document).ready(function () {
            // Initialize the FabricUI notification mechanism and hide it
            var element = document.querySelector('.ms-MessageBanner');
            messageBanner = new fabric.MessageBanner(element);
            messageBanner.hideBanner();

            // If not using Word 2016, use fallback logic.
            if (!Office.context.requirements.isSetSupported('WordApi', '1.1')) {
                $("#template-description").text("This sample displays the selected text.");
                $('#button-text').text("Display!");
                $('#button-desc').text("Display the selected text");

                $('#highlight-button').click(
                    displaySelectedText);
                return;
            }

            // Add a click event handler for the highlight button.
            $('#create-doc').click(CreateDocument);
        });
    };

    //$$(Helper function for treating errors, $loc_script_taskpane_home_js_comment34$)$$
    function errorHandler(error) {
        // $$(Always be sure to catch any accumulated errors that bubble up from the Word.run execution., $loc_script_taskpane_home_js_comment35$)$$
        showNotification("Error:", error);
        console.log("Error: " + error);
        if (error instanceof OfficeExtension.Error) {
            console.log("Debug info: " + JSON.stringify(error.debugInfo));
        }
    }

    // Helper function for displaying notifications
    function showNotification(header, content) {
        $("#notificationHeader").text(header);
        $("#notificationBody").text(content);
        messageBanner.showBanner();
        messageBanner.toggleExpansion();
    }
    // Reads data from current document selection and displays a notification
    function CreateDocument() {
        Word.run(function (context) {

            $("#notification").text("Request sent...")

            var dataToPassToService = {
                ID: $('#seg-id').val(),
                Prefill: $('#prefill').val()
            };

            $.ajax({
                url: 'api/ForteServer',
                type: 'POST',
                data: JSON.stringify(dataToPassToService),
                contentType: 'application/json;charset=utf-8'
            }).done(function (data) {
                context.document.body.insertBreak(Word.BreakType.next, Word.InsertLocation.end)
                context.document.body.insertFileFromBase64(data, "End")
                context.sync()
                $("#notification").text("Request Complete.")
            }).fail(function (status) {
                $("#notification").text("Request failed.")
            }).always(function () {
                // placeholder
            });
        })
    }
})();
